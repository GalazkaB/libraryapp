package app;

public enum Option {
    EXIT(0, "Wyjście z programu."),
    ADD_BOOK(1, "Dodanie książki."),
    ADD_MAGAZINE(2, "Dodanie magazynu lub gazety."),
    PRINT_BOOKS(3, "Wyświetlenie dostępnych książek."),
    PRINT_MAGAZINES(4, "Wyświetlenie dostępnych magazynów i gazet.");


    int optionNo;
    String optionDescription;

    public String getOptionDescription() {
        return optionDescription;
    }
    private int getOptionNo(){
        return optionNo;
    }

    private Option(int optionNo, String optionDescription){
        this.optionNo = optionNo;
        this.optionDescription = optionDescription;
    }

    @Override
    public String toString() {
        return optionNo + " - " + optionDescription;
    }

    public static Option createFromInt(int option) {
        return Option.values()[option];
    }
}




